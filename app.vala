using Gtk;
using GXml;
using Outliner;

int main(string[] args) {

	var app = new Gtk.Application("net.eniehack.outliner.vala", GLib.ApplicationFlags.FLAGS_NONE);

	app.activate.connect(() => {
		var window = new Gtk.ApplicationWindow(app);
		window.title = "TODO list";


		var treestore = new TreeStore(3, Type.STRING, Type.BOOLEAN, Type.STRING);

		var treeview = new TreeView();
		treeview.set_headers_visible(false);
		treeview.set_reorderable(true);
		treeview.set_model(treestore);

		var nodeid_cell = new CellRendererText();
		var nodeid_column = new TreeViewColumn.with_attributes("nodeid", nodeid_cell, "text", 0, null);
		nodeid_column.visible = false;
		var checkbox_cell = new CellRendererToggle();
		var check_column = new TreeViewColumn.with_attributes("checkbox", checkbox_cell, "active", 1, null);
		var content_cell = new CellRendererText();
		content_cell.editable = true;

		treeview.append_column(nodeid_column);
		treeview.append_column(check_column);
		treeview.insert_column_with_data_func(-1, "content", content_cell, (column, cell, store, iter) => {
			var text_renderer = (CellRendererText)cell;
			Value val;
			store.get_value(iter, 2, out val);
			text_renderer.markup = (string)val;
		});

		checkbox_cell.toggled.connect((path) => {
			TreeIter iter;
			var val = Value(typeof(bool));
			treestore.get_iter_from_string(out iter, path);
			treestore.get_value(iter, 1, out val);
			treestore.set_value(iter, 1, ! (val.get_boolean()));
		});

		content_cell.edited.connect((path, new_text) => {
			TreeIter iter;
			treestore.get_iter_from_string(out iter, path);
			treestore.set_value(iter, 2, new_text);
		});

		var scrolled_window = new ScrolledWindow();
		scrolled_window.set_child(treeview);
		scrolled_window.set_policy(PolicyType.AUTOMATIC, PolicyType.AUTOMATIC);
		scrolled_window.set_min_content_height(300);
		scrolled_window.set_max_content_height(900);
		scrolled_window.set_propagate_natural_height(true);
		scrolled_window.set_propagate_natural_width(true);

		var entry = new Entry();
		entry.set_size_request(120, -1);

		var hbox = new Box(Orientation.HORIZONTAL, 5);
		var file_open_btn = new Button.with_label("open");
		file_open_btn.clicked.connect(() => {
			var filedialog = new FileChooserNative("open", window, FileChooserAction.OPEN, "open", "cancel");
			//var filedialog = new FileChooserDialog("open", window, FileChooserAction.OPEN, "cancel", ResponseType.CANCEL, "open", ResponseType.ACCEPT);
			filedialog.visible = true;
			filedialog.response.connect((dialog, resp) => {
				var file = ((FileChooser)dialog).get_file();
				filedialog.visible = false;
				Outliner.DocumentReader docs = null;

				try {
					var xml = new XDocument.from_file(file);
					docs = new Outliner.DocumentReader(xml);
					stdout.printf("outliner.document initialized.");
				} catch (Outliner.FileError err) {
					stderr.printf("%s", err.message);
				} catch (GLib.Error err) {
					stderr.printf("initial error: %s", err.message);
				}
				docs.write(ref treestore);
			});
		});
		var file_save_btn = new Button.with_label("save");
		file_save_btn.clicked.connect(() => {
			var filedialog = new FileChooserNative("save as", window, FileChooserAction.SAVE, "save", "cancel");
			//var filedialog = new FileChooserDialog("open", window, FileChooserAction.OPEN, "cancel", ResponseType.CANCEL, "open", ResponseType.ACCEPT);
			filedialog.visible = true;
			filedialog.response.connect((dialog, resp) => {
				stdout.printf("file_save start\n");
				File? file;
				file = ((FileChooser)dialog).get_file();
				filedialog.visible = false;

				OutputStream filestream = null;
				Outliner.OutlinerXmlWriter docs = null;
				stdout.printf("file_save init end\n");
				if (file.query_exists()) {
					try {
						docs = new Outliner.OutlinerXmlWriter(treestore);
						stdout.printf("outliner.document initialized.");
						docs.write_file(file);
					} catch (GLib.Error err) {
						stderr.printf("initial error: %s", err.message);
					}
				} else {
					try {
						filestream = file.create(FileCreateFlags.NONE, null);
						docs = new Outliner.OutlinerXmlWriter(treestore);
						docs.write_stream(filestream);
					} catch (GLib.Error err) {
						stderr.printf("initial error: %s", err.message);
					}
				}
				file.unref();
				stdout.printf("file_save end\n");
			});
		});
		var add_btn = new Button.with_label("Add");
		add_btn.clicked.connect(() => {
			TreeIter iter;
			string val;

			if (entry.text.length <= 0) {
				return;
			}

			val = entry.text;
			treestore.append(out iter, null);
			treestore.set_value(iter, 0, Uuid.string_random());
			treestore.set_value(iter, 2, val);
			entry.text = "";
		});

		hbox.append(file_open_btn);
		hbox.append(file_save_btn);
		hbox.append(add_btn);
		hbox.append(entry);

		var remove_btn = new Button.with_label("Remove");
		remove_btn.clicked.connect(() => {
			TreeIter iter;
			if (! treestore.get_iter_first(out iter) ) {
				return;
			}

			var selection = treeview.get_selection();
			if (selection is TreeSelection) {
				selection.get_selected(null, out iter);
				treestore.remove(ref iter);
			}
		});
		hbox.append(remove_btn);

		var child_btn = new Button.with_label("Child");
		child_btn.clicked.connect(() => {
			TreeIter parent;
			TreeIter child;
			string val;

			var selection = treeview.get_selection();
			if (selection is TreeSelection) {
				if (! treestore.get_iter_first(out child) ) {
					return;
				}

				if (entry.text.length <= 0) {
					return;
				}

				val = entry.text;
				selection.get_selected(null, out parent);
				treestore.append(out child, parent);
				treestore.set_value(child, 0, Uuid.string_random());
				treestore.set_value(child, 2, val);
				entry.text = "";
			}
		});
		hbox.append(child_btn);

		var removeall_btn = new Button.with_label("Remove All");
		removeall_btn.clicked.connect(() => {
			TreeIter iter;
			if ( ! treestore.get_iter_first(out iter) ) {
				return;
			}
			treestore.clear();
		});
		hbox.append(removeall_btn);

		var vbox = new Box(Orientation.VERTICAL, 0);
		vbox.set_homogeneous(false);
		vbox.append(scrolled_window);
		vbox.append(hbox);
		window.set_child(vbox);

		window.present();
	});

	return app.run(args);
}
