namespace Outliner {
	using Gee;
	using Gtk;
	using GXml;

	public class OutlinerXmlWriter {
		DocumentWriter writer {get; set;}

		public OutlinerXmlWriter(Gtk.TreeStore store) {
			//this.document = new GXml.XDocument();
			writer = new DocumentWriter(store);
		}

		public void write_stream(OutputStream stream) throws GLib.Error {
			//document.set_namespace("http://eniehack.gitlab.io/outliner-spec/ns/0.1.0/", null);
			//stdout.printf("namespace");

			/*
			var roots_node = new Xml.Node(null, "roots");
			var roots_elem = new GXml.XElement(document, roots_node);
			document.append_child(roots_elem);
			*/
			stream.printf(null, null, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
			stream.printf(null, null, "<outliner version=\"0.1.0\">\n");
			stream.printf(null, null, "<roots>\n");
			foreach (var root_id in writer.top_nodes) {
				stream.printf(null, null, "<root ref=\"%s\" />\n", root_id);
				/*
				root_elem.class_name = "root";
				root_elem.set_attribute("ref", root_id);
				roots_elem.append_child(root_elem);
				*/
			}
			stream.printf(null, null, "</roots>\n");

			/*
			var nodes_node = new Xml.Node(null, "nodes");
			var nodes_elem = new GXml.XElement(document, nodes_node);
			document.append_child(nodes_elem);
			*/
			stream.printf(null, null, "<nodes>\n");
			foreach (var node in writer.nodes) {
				/*
				var root_elem = new Element();
				stdout.printf("<root ref=\"%s\">\n</root>\n", root_id);
				root_elem.class_name = "root";
				root_elem.set_attribute("ref", root_id);
				roots_elem.append_child(root_elem);
				stdout.printf("root_elem");
				*/

				/*
				var node = nodes.@get(root_id);
				var node_elem = new Element();
				node_elem.class_name = "node";
				node_elem.set_attribute("id", node.id);
				node_elem.set_attribute("done", node.done.to_string());
				stdout.printf("node_elem");
				var content_elem = new Element();
				content_elem.text_content = node.content;
				node_elem.append_child(content_elem);
				*/
				var id = node.key;
				var body = node.value;
				stream.printf(null, null, "<node id=\"%s\" done=\"%s\">\n", id, body.done.to_string());
				stream.printf(null, null, "<content>%s</content>\n", body.content);
				if (1 <= body.children.size) {
					stream.printf(null, null, "<children>\n");
					foreach ( var child in body.children ) {
						stream.printf(null, null, "<child ref=\"%s\" />\n", child);
					}
					stream.printf(null, null, "</children>\n");
				}
				stream.printf(null, null, "</node>\n");
			}
			stream.printf(null, null, "</nodes>\n");
			stream.printf(null, null, "</outliner>\n");
			stream.close();
		}

		public void write_file(File file) throws GLib.Error {
			this.write_stream(file.replace(null, false, FileCreateFlags.NONE));
			return;
		}

		public string write_string() throws GLib.Error {
			var src_stream = new MemoryOutputStream(null);
			write_stream(src_stream);
			var dst_stream = new MemoryInputStream();
			src_stream.splice(dst_stream, 0);
			var builder = new StringBuilder();
			uint8 buf[100];
			ssize_t size;

			while ((size = dst_stream.read(buf)) > 0) {
				builder.append_len((string)buf, size);
			}
			return builder.str;
		}
	}
}
