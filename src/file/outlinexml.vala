using GXml;
using Gee;
using Gtk;

namespace Outliner {

	public errordomain FileError {
		InvaildFileFormat
	}

	public class ReaderNode : GLib.Object {
		public string id { get; set; }
		public Gee.ArrayList<string> children { get; set; }
		public DomElement content { get; set; }
		public bool done { get; set; default = false;}
		public bool is_top_node {get; set; default = false;}

		public ReaderNode() {
			this.children = new Gee.ArrayList<string> ();
		}
	}

	public class DocumentReader : GLib.Object {
		public Gee.ArrayList<string> top_nodes {get; set;}
		public Gee.HashMap<string, ReaderNode> nodes {get; set;}

		public DocumentReader(XDocument xml) throws FileError {
			this.top_nodes = new Gee.ArrayList<string> ();
			this.nodes = new Gee.HashMap<string, ReaderNode>();

			XPathObject tops = null;
			/*
			var resolver = new Gee.HashMap<string, string> ();
			resolver.@set("", "http://eniehack.gitlab.io/outliner-spec/ns/0.1.0/");
			*/

			try {
				tops = xml.evaluate("/outliner/roots/root");
			} catch (XPathObjectError err) {
				throw new FileError.InvaildFileFormat("root not found. maybe invaild file format.");
			}

			foreach (DomElement elem in tops.nodeset) {
				stdout.printf("top attr[id]: %s\n", elem.get_attribute("ref"));
				this.top_nodes.add( elem.get_attribute("ref") );
			}
			stdout.printf("%d", tops.nodeset.length);

			XPathObject nodes = null;
			try {
				nodes = xml.evaluate("/outliner/nodes/node");
			} catch (XPathObjectError err) {
				throw new FileError.InvaildFileFormat("node not found. maybe invaild file format.");
			}
			stdout.printf("%d", nodes.nodeset.length);

			foreach (var elem in nodes.nodeset) {
				var node = new ReaderNode();
				stdout.printf("------\n");
				stdout.printf("attr[id]: %s\n", elem.get_attribute("id"));
				stdout.printf("node name: %s\n", elem.tag_name);

				if (this.top_nodes.contains(elem.get_attribute("id"))) {
					node.is_top_node = true;
				}

				node.content = (DomElement)elem.get_elements_by_tag_name("content").item(0);

				foreach (var item in elem.child_nodes) {
					if ((NodeType.ELEMENT in (NodeType)item.node_type)
						 && (item.node_name == "children")) {

						foreach(var child in ((DomElement)item).children) {
							if ((NodeType.ELEMENT in (NodeType)child.node_type)){
								var elem_child = (DomElement)child;
								if ((child.node_name == "child") && elem_child.has_attribute("ref")) {
									stdout.printf("%s 's children: %s\n", elem.get_attribute("id"), elem_child.write_string());
									node.children.add( elem_child.get_attribute("ref") );
								}
							}
						}

					}
				}

				stdout.printf("%s's child\n", elem.get_attribute("id"));
				foreach (var item in node.children) {
					if (item == null) {
						stdout.printf("null");
					}
					stdout.printf("* child: %s\n", item);
				}

				this.nodes.set(elem.get_attribute("id"), node);
				stdout.printf("------\n");
			}
		}

		public void write(ref Gtk.TreeStore store) {
			foreach ( var item_id in this.top_nodes ) {
				TreeIter iter;
				var item = this.nodes.get(item_id);
				var content_nodes = this.nodes.get(item_id).content;
				store.append(out iter, null);
				store.set_value(iter, 0, item_id);
				store.set_value(iter, 1, false);

				foreach (var child in content_nodes.child_nodes ) {
					stdout.printf("nodevalue: %s\n", child.node_value);
					string content = "";
					switch ((NodeType)child.node_type) {
				    case NodeType.TEXT:
					    content = content.concat(child.node_value);
						stdout.printf("TEXT: %s\n", child.node_value);
						break;
				    case NodeType.ELEMENT:
						content = content.concat(((DomElement)child).write_string());
						stdout.printf("ELEMENT(%s): %s/%s\n", ((DomElement)child).write_string(), child.node_name, child.node_value);
						break;
				    default:
						stdout.printf("default\n");
						break;
					}
					store.set_value(iter, 2, content);
				}
				this.write_children(ref store, iter, item.children);
			}
		}

		void write_children(ref Gtk.TreeStore store, TreeIter parent,Gee.ArrayList<string> children) {
			foreach (var child_id in children) {
				TreeIter iter;
				store.append(out iter, parent);
				store.set_value(iter, 0, child_id);
				store.set_value(iter, 1, false);
				var content_nodes = this.nodes.get(child_id).content;
				string content = "";
				foreach (var child in content_nodes.child_nodes ) {
					stdout.printf("nodevalue: %s\n", child.node_value);
					switch ((NodeType)child.node_type) {
					case NodeType.TEXT:
					    content = content.concat(child.node_value);
						stdout.printf("TEXT: %s\n", child.node_value);
						break;
				    case NodeType.ELEMENT:
						content = content.concat(((DomElement)child).write_string());
						stdout.printf("ELEMENT(%s): %s/%s\n", ((DomElement)child).write_string(), child.node_name, child.node_value);
						break;
				    default:
						stdout.printf("default\n");
						break;
					}
				}
				store.set_value(iter, 2, content);
				if (this.nodes.get(child_id).children.size != 0) {
					write_children(ref store, iter, this.nodes.get(child_id).children);
				}
			}
		}
	}
}
