namespace Outliner {
	using Gtk;
	using Gee;
	using GXml;

	public class WriterNode : GLib.Object {
		public string id { get; set; }
		public Gee.ArrayList<string> children { get; set; }
		public string content { get; set; }
		public bool done { get; set; default = false;}
		public bool is_top_node {get; set; default = false;}

		public WriterNode() {
			this.children = new Gee.ArrayList<string> ();
		}
	}


	public class DocumentWriter : GLib.Object {
		public Gee.ArrayList<string> top_nodes {get; set;}
		public Gee.HashMap<string, Outliner.WriterNode> nodes {get; set;}
		Gtk.TreeStore store;

		public DocumentWriter(Gtk.TreeStore store) {
			TreeIter iter;
			this.store = store;
			top_nodes = new Gee.ArrayList<string>();
			nodes = new Gee.HashMap<string, Outliner.WriterNode>();

			store.get_iter_first(out iter);
			do {
				Outliner.WriterNode node = new Outliner.WriterNode();
				Value val;
				store.get_value(iter, 0, out val);
				string id = val.get_string();
				stdout.printf("treestore column 0: %s\n", id);
				store.get_value(iter, 1, out val);
				node.done = val.get_boolean();
				stdout.printf("treestore column 1: %s\n", node.done.to_string());
				store.get_value(iter, 2, out val);
				node.content = val.get_string();
				stdout.printf("treestore column 2: %s\n", node.content);

				node.is_top_node = true;
				top_nodes.add(id);
				nodes.@set(id, node);
				stdout.printf("node set %s\n", id);

				TreeIter child;
				if (store.iter_children(out child, iter) ) {
					do {
						Outliner.WriterNode child_node = new Outliner.WriterNode();

						Value ch_val;
						string child_id;

						store.get_value(child, 0, out ch_val);
						child_id = ch_val.get_string();
						stdout.printf("treestore column 0: %s\n", child_id);

						store.get_value(child, 1, out ch_val);
						child_node.done = ch_val.get_boolean();
						stdout.printf("children: treestore column 1: %s\n", child_node.done.to_string());

						store.get_value(child, 2, out ch_val);
						child_node.content = ch_val.get_string();
						stdout.printf("children: treestore column 2: %s\n", child_node.content);

						nodes.@get(id).children.add(child_id);
						stdout.printf("id(%s)'s children: %s\n", id, child_id);
						nodes.@set(child_id, child_node);
						if (store.iter_has_child(child)) {
							this.load_children(child, child_id);
						}
					} while ( store.iter_next(ref child) );
				}
			} while ( store.iter_next(ref iter) );
		}

		void load_children(Gtk.TreeIter parent, string parent_id) {
			TreeIter iter;

			store.iter_children(out iter, parent);
			do {
				Outliner.WriterNode node = new Outliner.WriterNode();

				Value val;
				store.get_value(iter, 0, out val);
				string id = val.get_string();
				stdout.printf("treestore column 0: %s\n", id);
				store.get_value(iter, 1, out val);
				node.done = val.get_boolean();
				stdout.printf("load_children: treestore column 1: %s\n", node.done.to_string());
				store.get_value(iter, 2, out val);
				node.content = val.get_string();
				stdout.printf("load_children: treestore column 2: %s\n", node.content);

				nodes.@get(parent_id).children.add(id);
				stdout.printf("id(%s)'s children: %s\n", parent_id, id);
				nodes.@set(id, node);
				if (store.iter_has_child(iter)) {
					this.load_children(iter, id);
				}
			} while ( store.iter_next(ref iter));
		}

		public virtual void write_file(File file) throws GLib.Error {}

		public virtual string write_string() throws GLib.Error {
			return "";
		}

		public virtual void write_stream(OutputStream stream) throws GLib.Error {}
	}
}
